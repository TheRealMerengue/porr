#ifndef AES_MSG
#define AES_MSG

#include <string>
#include <cstring>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cmath>

#include "types.hpp"

using namespace std;

class Text
{
public:
	Text();
	~Text();

	int load_from_file(string);
	int save_to_file(string);
	void set_content(byte*, int);
	int get_size();
	byte get_byte(int);

private:
	byte* content;
	string file_name;
	int size;
};

Text::Text()
{
	size = -1;
	file_name = "";
	content = NULL;
}

Text::~Text()
{
	if (content != NULL) {
		delete[] content;
	}
}

int Text::load_from_file(string filename)
{
	ifstream file(filename.c_str(), ios::binary);
	if (!file.is_open()) {
		return -1;
	}

	file_name = filename;
	file.seekg(0, ios::end); 	
	size = file.tellg();	
	file.seekg(0, ios::beg);
	
	if (content != NULL)
		delete[] content;

	content = new byte[size];
	
	file.read((char*)content, size);

	file.close();

	cout << "File " << file_name << " loaded. " << size << " bytes loaded" << endl;
	return 0;
}


int Text::save_to_file(string filename)
{
	file_name = filename;

	ofstream file(file_name.c_str(), ios::binary);

	file.write((char*)content, size);
	file.close();

	cout << "File " << file_name << " saved. " << size << " bytes saved" << endl;
	return 0;
}

byte Text::get_byte(int pos)
{
	return content[pos];
}

int Text::get_size()
{
	return size;
}

void Text::set_content(byte* data, int size)
{
	if (content != NULL)
		delete[] content;

	content = new byte[size];
	memcpy(content, data, size);
	
	this->size = size;
}

#endif
