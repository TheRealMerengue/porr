#include "types.hpp"
#include "text.hpp"
#include <string>

// The number of columns comprising a state in AES. This is a constant in AES. Value=4
#define Nb 4

// Storing the key
byte key[4][4];

byte block[4][4];

byte round_key[4][60];

int Nr = 0;

int Nk = 0;

byte state[4][4];
